﻿namespace Blum.Access.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BlumAccessv1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UsersLogin",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 255),
                        Senha = c.String(nullable: false, maxLength: 8),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UsersLogin");
        }
    }
}
