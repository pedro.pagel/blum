﻿namespace Blum.Access.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BAIRROS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CidadesId = c.Int(nullable: false),
                        EstadosId = c.Int(nullable: false),
                        PaisesId = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 255),
                        Sigla = c.String(maxLength: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CIDADES", t => t.CidadesId)
                .ForeignKey("dbo.ESTADOS", t => t.EstadosId)
                .ForeignKey("dbo.PAISES", t => t.PaisesId)
                .Index(t => t.CidadesId)
                .Index(t => t.EstadosId)
                .Index(t => t.PaisesId);
            
            CreateTable(
                "dbo.CIDADES",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EstadosId = c.Int(nullable: false),
                        PaisesId = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 255),
                        Sigla = c.String(nullable: false, maxLength: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ESTADOS", t => t.EstadosId)
                .ForeignKey("dbo.PAISES", t => t.PaisesId)
                .Index(t => t.EstadosId)
                .Index(t => t.PaisesId);
            
            CreateTable(
                "dbo.ESTADOS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaisesId = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 255),
                        Sigla = c.String(nullable: false, maxLength: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PAISES", t => t.PaisesId)
                .Index(t => t.PaisesId);
            
            CreateTable(
                "dbo.PAISES",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 255),
                        Sigla = c.String(nullable: false, maxLength: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CADASTROS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaisesId = c.Int(nullable: false),
                        EstadosId = c.Int(nullable: false),
                        CidadesId = c.Int(nullable: false),
                        BairrosId = c.Int(nullable: false),
                        Logradouro = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BAIRROS", t => t.BairrosId)
                .ForeignKey("dbo.CIDADES", t => t.CidadesId)
                .ForeignKey("dbo.ESTADOS", t => t.EstadosId)
                .ForeignKey("dbo.PAISES", t => t.PaisesId)
                .Index(t => t.PaisesId)
                .Index(t => t.EstadosId)
                .Index(t => t.CidadesId)
                .Index(t => t.BairrosId);
            
            CreateTable(
                "dbo.LAYOUT_CATEGORIAS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LayoutMercadosId = c.Int(nullable: false),
                        URL = c.String(nullable: false),
                        Categoria = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LAYOUT_MERCADOS", t => t.LayoutMercadosId)
                .Index(t => t.LayoutMercadosId);
            
            CreateTable(
                "dbo.LAYOUT_MERCADOS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        URL = c.String(nullable: false),
                        MercadosId = c.Int(nullable: false),
                        PossuiNumeroPagina = c.Boolean(nullable: false),
                        QuantidadePorPagina = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MERCADOS", t => t.MercadosId)
                .Index(t => t.MercadosId);
            
            CreateTable(
                "dbo.MERCADOS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PRODUTOS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MercadosId = c.Int(nullable: false),
                        SubCategoriaId = c.Int(nullable: false),
                        MarcasId = c.Int(),
                        Nome = c.String(nullable: false, maxLength: 255),
                        Categoria = c.Int(nullable: false),
                        UnidadeMedida = c.Int(nullable: false),
                        Preco = c.Decimal(nullable: false, storeType: "money"),
                        PrecoOriginal = c.Decimal(nullable: false, storeType: "money"),
                        PrecoExclusivo = c.Decimal(nullable: false, storeType: "money"),
                        UltimoPreco = c.Decimal(nullable: false, storeType: "money"),
                        UltimoPrecoOriginal = c.Decimal(nullable: false, storeType: "money"),
                        UltimoPrecoExclusivo = c.Decimal(nullable: false, storeType: "money"),
                        IdAnuncio = c.String(maxLength: 255),
                        Situacao = c.Int(nullable: false),
                        DataInclusao = c.DateTime(nullable: false),
                        DataAlteracao = c.DateTime(),
                        Kit = c.Byte(nullable: false),
                        Popularidade = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MERCADOS", t => t.MercadosId)
                .ForeignKey("dbo.SUBCATEGORIAS", t => t.SubCategoriaId)
                .Index(t => t.MercadosId)
                .Index(t => t.SubCategoriaId);
            
            CreateTable(
                "dbo.SUBCATEGORIAS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 75),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LAYOUT_PRODUTOS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 255),
                        Chave = c.String(nullable: false, maxLength: 50),
                        PrecoFinal = c.String(nullable: false, maxLength: 50),
                        PrecoOriginal = c.String(nullable: false, maxLength: 50),
                        PrecoExclusivo = c.String(nullable: false, maxLength: 50),
                        TagPrecoOriginal = c.String(nullable: false, maxLength: 10),
                        TagPrecoFinal = c.String(nullable: false, maxLength: 10),
                        TagPrecoExclusivo = c.String(nullable: false, maxLength: 10),
                        TagNome = c.String(nullable: false, maxLength: 10),
                        LayoutMercadosId = c.Int(nullable: false),
                        IdAnuncio = c.String(nullable: false, maxLength: 50),
                        TagAnuncio = c.String(nullable: false, maxLength: 10),
                        IdIndisponivel = c.String(nullable: false, maxLength: 255),
                        TagIndisponivel = c.String(nullable: false, maxLength: 10),
                        TagChave = c.String(maxLength: 10),
                        ConteudoChave = c.String(maxLength: 50),
                        NodeUnico = c.String(maxLength: 250),
                        PrecoOriginalXPath = c.String(maxLength: 250),
                        BadOuterHtmlPreco = c.String(maxLength: 250),
                        LayoutCategorias_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LAYOUT_MERCADOS", t => t.LayoutMercadosId)
                .ForeignKey("dbo.LAYOUT_CATEGORIAS", t => t.LayoutCategorias_Id)
                .Index(t => t.LayoutMercadosId)
                .Index(t => t.LayoutCategorias_Id);
            
            CreateTable(
                "dbo.LISTA_CHAVES",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ListasId = c.Int(nullable: false),
                        ChaveProduto = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LISTAS", t => t.ListasId)
                .Index(t => t.ListasId);
            
            CreateTable(
                "dbo.LISTAS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PessoasId = c.Int(nullable: false),
                        DataInclusao = c.DateTime(nullable: false),
                        DataAlteracao = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PESSOAS", t => t.PessoasId)
                .Index(t => t.PessoasId);
            
            CreateTable(
                "dbo.PESSOAS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 255),
                        Sexo = c.Int(nullable: false),
                        EstadoCivil = c.Int(),
                        Email = c.String(nullable: false, maxLength: 255),
                        CPF = c.String(maxLength: 15),
                        DataNascimento = c.DateTime(nullable: false),
                        DataInclusao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LISTA_PRODUTOS",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProdutosId = c.Int(nullable: false),
                        ListaChaveId = c.Int(),
                        ListasId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LISTA_CHAVES", t => t.ListaChaveId)
                .ForeignKey("dbo.LISTAS", t => t.ListasId)
                .ForeignKey("dbo.PRODUTOS", t => t.ProdutosId)
                .Index(t => t.ProdutosId)
                .Index(t => t.ListaChaveId)
                .Index(t => t.ListasId);
            
            CreateTable(
                "dbo.REQUISICAO_VALORES",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Chave = c.String(nullable: false, maxLength: 75),
                        Valor = c.String(nullable: false, maxLength: 75),
                        LayoutMercadosId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LAYOUT_MERCADOS", t => t.LayoutMercadosId)
                .Index(t => t.LayoutMercadosId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.REQUISICAO_VALORES", "LayoutMercadosId", "dbo.LAYOUT_MERCADOS");
            DropForeignKey("dbo.LISTA_PRODUTOS", "ProdutosId", "dbo.PRODUTOS");
            DropForeignKey("dbo.LISTA_PRODUTOS", "ListasId", "dbo.LISTAS");
            DropForeignKey("dbo.LISTA_PRODUTOS", "ListaChaveId", "dbo.LISTA_CHAVES");
            DropForeignKey("dbo.LISTA_CHAVES", "ListasId", "dbo.LISTAS");
            DropForeignKey("dbo.LISTAS", "PessoasId", "dbo.PESSOAS");
            DropForeignKey("dbo.LAYOUT_PRODUTOS", "LayoutCategorias_Id", "dbo.LAYOUT_CATEGORIAS");
            DropForeignKey("dbo.LAYOUT_PRODUTOS", "LayoutMercadosId", "dbo.LAYOUT_MERCADOS");
            DropForeignKey("dbo.LAYOUT_CATEGORIAS", "LayoutMercadosId", "dbo.LAYOUT_MERCADOS");
            DropForeignKey("dbo.LAYOUT_MERCADOS", "MercadosId", "dbo.MERCADOS");
            DropForeignKey("dbo.PRODUTOS", "SubCategoriaId", "dbo.SUBCATEGORIAS");
            DropForeignKey("dbo.PRODUTOS", "MercadosId", "dbo.MERCADOS");
            DropForeignKey("dbo.CADASTROS", "PaisesId", "dbo.PAISES");
            DropForeignKey("dbo.CADASTROS", "EstadosId", "dbo.ESTADOS");
            DropForeignKey("dbo.CADASTROS", "CidadesId", "dbo.CIDADES");
            DropForeignKey("dbo.CADASTROS", "BairrosId", "dbo.BAIRROS");
            DropForeignKey("dbo.BAIRROS", "PaisesId", "dbo.PAISES");
            DropForeignKey("dbo.BAIRROS", "EstadosId", "dbo.ESTADOS");
            DropForeignKey("dbo.BAIRROS", "CidadesId", "dbo.CIDADES");
            DropForeignKey("dbo.CIDADES", "PaisesId", "dbo.PAISES");
            DropForeignKey("dbo.ESTADOS", "PaisesId", "dbo.PAISES");
            DropForeignKey("dbo.CIDADES", "EstadosId", "dbo.ESTADOS");
            DropIndex("dbo.REQUISICAO_VALORES", new[] { "LayoutMercadosId" });
            DropIndex("dbo.LISTA_PRODUTOS", new[] { "ListasId" });
            DropIndex("dbo.LISTA_PRODUTOS", new[] { "ListaChaveId" });
            DropIndex("dbo.LISTA_PRODUTOS", new[] { "ProdutosId" });
            DropIndex("dbo.LISTAS", new[] { "PessoasId" });
            DropIndex("dbo.LISTA_CHAVES", new[] { "ListasId" });
            DropIndex("dbo.LAYOUT_PRODUTOS", new[] { "LayoutCategorias_Id" });
            DropIndex("dbo.LAYOUT_PRODUTOS", new[] { "LayoutMercadosId" });
            DropIndex("dbo.PRODUTOS", new[] { "SubCategoriaId" });
            DropIndex("dbo.PRODUTOS", new[] { "MercadosId" });
            DropIndex("dbo.LAYOUT_MERCADOS", new[] { "MercadosId" });
            DropIndex("dbo.LAYOUT_CATEGORIAS", new[] { "LayoutMercadosId" });
            DropIndex("dbo.CADASTROS", new[] { "BairrosId" });
            DropIndex("dbo.CADASTROS", new[] { "CidadesId" });
            DropIndex("dbo.CADASTROS", new[] { "EstadosId" });
            DropIndex("dbo.CADASTROS", new[] { "PaisesId" });
            DropIndex("dbo.ESTADOS", new[] { "PaisesId" });
            DropIndex("dbo.CIDADES", new[] { "PaisesId" });
            DropIndex("dbo.CIDADES", new[] { "EstadosId" });
            DropIndex("dbo.BAIRROS", new[] { "PaisesId" });
            DropIndex("dbo.BAIRROS", new[] { "EstadosId" });
            DropIndex("dbo.BAIRROS", new[] { "CidadesId" });
            DropTable("dbo.REQUISICAO_VALORES");
            DropTable("dbo.LISTA_PRODUTOS");
            DropTable("dbo.PESSOAS");
            DropTable("dbo.LISTAS");
            DropTable("dbo.LISTA_CHAVES");
            DropTable("dbo.LAYOUT_PRODUTOS");
            DropTable("dbo.SUBCATEGORIAS");
            DropTable("dbo.PRODUTOS");
            DropTable("dbo.MERCADOS");
            DropTable("dbo.LAYOUT_MERCADOS");
            DropTable("dbo.LAYOUT_CATEGORIAS");
            DropTable("dbo.CADASTROS");
            DropTable("dbo.PAISES");
            DropTable("dbo.ESTADOS");
            DropTable("dbo.CIDADES");
            DropTable("dbo.BAIRROS");
        }
    }
}
