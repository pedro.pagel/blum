﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Reflection;

namespace Blum.Access
{
    public class EntityBase
    {
        private object GetIdValue()
        {
            Type type = this.GetType();
            PropertyInfo propInfo = type.GetProperty("Id");
            return propInfo.GetValue(this);
        }

        private void SetValue(EntityBase entity)
        {
            Type type = entity.GetType();
            PropertyInfo propInfo = type.GetProperty("Id");
            propInfo.SetValue(entity, GetIdValue());
        }

        public void Update(EntityBase entity)
        {
            this.SetValue(entity);
            BlumAccess.Db.Entry(this).CurrentValues.SetValues(entity);
        }

        public void Insert()
        {
            var entity = BlumAccess.Db.Set(this.GetType());
            entity.Add(this);
        }

        public void Delete(EntityBase remove)
        {
            var entity = BlumAccess.Db.Set(this.GetType());
            entity.Remove(remove);
        }

        public List<object> GetAll()
        {
            return BlumAccess.Db.Set(GetType()).ToListAsync().Result;
        }

        public object Get(int id)
        {
            return BlumAccess.Db.Set(GetType()).Find(id);
        }

        public List<TEntity> GetAll<TEntity>() where TEntity : class
        {
            return BlumAccess.Db.Set<TEntity>().ToListAsync().Result;
        }

        public object Get<TEntity>(int id) where TEntity : class
        {
            return BlumAccess.Db.Set<TEntity>().Find(id);
        }

        public void Clone(object entity)
        {
            Type type = entity.GetType();
            var propInfo = type.GetProperties();

            foreach (PropertyInfo propertyInfo in propInfo)
            {
                PropertyInfo prop = this.GetType().GetProperty(propertyInfo.Name);

                if (prop != null)
                {
                    prop.SetValue(this, propertyInfo.GetValue(entity));
                }
            }
        }
    }
}
