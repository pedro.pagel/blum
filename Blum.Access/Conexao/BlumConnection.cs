﻿using Blum.Access.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Blum.Access
{
    public class BlumConnection : DbContext
    {
        public BlumConnection() : base("Data Source=DESKTOP-NVPBAQD\\SQLEXPRESS;Initial Catalog=Blum;Persist Security Info=False;User ID=sa;Password=Admin0132;MultipleActiveResultSets=True")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BlumConnection, Migrations.Configuration>());
            //Configuration.LazyLoadingEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            //modelBuilder.Entity<Produtos>().ToTable("Produtos");
            //modelBuilder.Entity<Mercados>().ToTable("Mercados");

            //modelBuilder.Entity<Produtos>().HasKey(c => new { c.MercadosId });
        }

        public DbSet<Paises> Pais { get; set; }
        public DbSet<Estados> Estados { get; set; }
        public DbSet<Cidades> Cidades { get; set; }
        public DbSet<Cadastros> Cadastro { get; set; }
        public DbSet<Bairros> Bairros { get; set; }
        public DbSet<Pessoas> Pessoas { get; set; }
        public DbSet<Listas> Listas { get; set; }
        public DbSet<Produtos> Produtos { get; set; }
        public DbSet<Mercados> Mercados { get; set; }
        public DbSet<LayoutMercados> LayoutMercados { get; set; }
        public DbSet<LayoutCategorias> LayoutCategorias { get; set; }
        public DbSet<LayoutProdutos> LayoutProdutos { get; set; }
        public DbSet<SubCategorias> SubCategorias { get; set; }
        public DbSet<RequisicaoValores> RequisicaoValores { get; set; }
        public DbSet<ListaProdutos> ListaProdutos { get; set; }
        public DbSet<ListaChaves> ListaChaves { get; set; }
        public DbSet<UsersLogin> UsersLogin { get; set; }
    }

    public static class BlumAccess
    {
        public static BlumConnection Db = null;
    }
}
