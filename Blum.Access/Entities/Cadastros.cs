﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("CADASTROS")]
    public class Cadastros
    {
        [Key]
        public int Id { get; set; }
        public int PaisesId { get; set; }
        public int EstadosId { get; set; }
        public int CidadesId { get; set; }
        public int BairrosId { get; set; }

        [StringLength(255), DisplayName("Logradouro")]
        public string Logradouro { get; set; }

        [ForeignKey("PaisesId")]
        public virtual Paises Paises { get; set; }
        [ForeignKey("EstadosId")]
        public virtual Estados Estados { get; set; }
        [ForeignKey("CidadesId")]
        public virtual Cidades Cidades { get; set; }
        [ForeignKey("BairrosId")]
        public virtual Bairros Bairros { get; set; }
    }
}
