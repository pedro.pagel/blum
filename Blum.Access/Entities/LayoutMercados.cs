﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("LAYOUT_MERCADOS")]
    public class LayoutMercados
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string URL { get; set; }

        public int MercadosId { get; set; }

        [ForeignKey("MercadosId")]
        public virtual Mercados Mercados { get; set; }

        [Required]
        public bool PossuiNumeroPagina { get; set; }

        public int QuantidadePorPagina { get; set; }
    }
}
