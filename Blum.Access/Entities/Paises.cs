﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("PAISES")]
    public class Paises
    {
        public Paises()
        {
            Estados = new HashSet<Estados>();
        }

        [Key]
        public int Id { get; set; }

        [Required, StringLength(255), DisplayName("Nome")]
        public string Nome { get; set; }

        [Required, StringLength(2), DisplayName("Sigla")]
        public string Sigla { get; set; }
        public virtual ICollection<Estados> Estados { get; set; }
    }
}
