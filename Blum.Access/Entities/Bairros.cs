﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("BAIRROS")]
    public class Bairros
    {
        [Key]
        public int Id { get; set; }
        public int CidadesId { get; set; }
        public int EstadosId { get; set; }
        public int PaisesId { get; set; }

        [Required, StringLength(255), DisplayName("Nome")]
        public string Nome { get; set; }

        [StringLength(2), DisplayName("Sigla")]
        public string Sigla { get; set; }

        [ForeignKey("CidadesId")]
        public virtual Cidades Cidade { get; set; }

        [ForeignKey("EstadosId")]
        public virtual Estados Estados { get; set; }

        [ForeignKey("PaisesId")]
        public virtual Paises Paises { get; set; }
    }
}
