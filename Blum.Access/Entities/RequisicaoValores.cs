﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("REQUISICAO_VALORES")]
    public class RequisicaoValores
    {
        [Key]
        public int Id { get; set; }

        [Required, StringLength(75)]
        public string Chave { get; set; }

        [Required, StringLength(75)]
        public string Valor { get; set; }

        public int LayoutMercadosId { get; set; }

        [ForeignKey("LayoutMercadosId")]
        public virtual LayoutMercados LayoutMercados { get; set; }
    }
}
