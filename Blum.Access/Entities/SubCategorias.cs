﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("SUBCATEGORIAS")]
    public class SubCategorias
    {
        [Key]
        public int Id { get; set; }

        [Required, StringLength(75)]
        public string Nome { get; set; }
    }
}
