﻿using Blum.Access.Entities.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blum.Access.Entities
{
    [Table("PESSOAS")]
    public class Pessoas : EntityBase
    {
        [Key]
        public int Id { get; set; }

        [Required, StringLength(255), DisplayName("Nome")]
        public string Nome { get; set; }
        public Sexo Sexo { get; set; }
        public EstadoCivil? EstadoCivil { get; set; }

        [Required, StringLength(255), DisplayName("E-mail")]
        public string Email { get; set; }

        [StringLength(15), DisplayName("CPF")]
        public string CPF { get; set; }

        [DisplayName("Nascimento")]
        public DateTime DataNascimento { get; set; }
        public DateTime DataInclusao { get; set; }
    }
}
