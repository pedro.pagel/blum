﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("MERCADOS")]
    public class Mercados
    {
        public Mercados()
        {
            Produtos = new HashSet<Produtos>();
        }

        [Key]
        public int Id { get; set; }

        [Required, StringLength(255), DisplayName("Nome")]
        public string Nome { get; set; }

        [ForeignKey("MercadosId")]
        public virtual ICollection<Produtos> Produtos { get; set; }
    }
}
