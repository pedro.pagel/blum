﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("LAYOUT_PRODUTOS")]
    public class LayoutProdutos
    {
        [Key]
        public int Id { get; set; }

        [Required, StringLength(255)]
        public string Nome { get; set; }

        [Required, StringLength(50)]
        public string Chave { get; set; }

        [Required, StringLength(50)]
        public string PrecoFinal { get; set; }

        [Required, StringLength(50)]
        public string PrecoOriginal { get; set; }

        [Required, StringLength(50)]
        public string PrecoExclusivo { get; set; }

        [Required, StringLength(10)]
        public string TagPrecoOriginal { get; set; }

        [Required, StringLength(10)]
        public string TagPrecoFinal { get; set; }

        [Required, StringLength(10)]
        public string TagPrecoExclusivo { get; set; }

        [Required, StringLength(10)]
        public string TagNome { get; set; }

        public int LayoutMercadosId { get; set; }

        [ForeignKey("LayoutMercadosId")]
        public virtual LayoutMercados LayoutMercados { get; set; }

        [Required, StringLength(50)]
        public string IdAnuncio { get; set; }

        [Required, StringLength(10)]
        public string TagAnuncio { get; set; }

        [Required, StringLength(255)]
        public string IdIndisponivel { get; set; }

        [Required, StringLength(10)]
        public string TagIndisponivel { get; set; }

        [StringLength(10)]
        public string TagChave { get; set; }

        [StringLength(50)]
        public string ConteudoChave { get; set; }

        [StringLength(250)]
        public string NodeUnico { get; set; }

        [StringLength(250)]
        public string PrecoOriginalXPath { get; set; }

        [StringLength(250)]
        public string BadOuterHtmlPreco { get; set; }
    }
}
