﻿using Blum.Access.Entities.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("LAYOUT_CATEGORIAS")]
    public class LayoutCategorias
    {
        public LayoutCategorias()
        {
            LayoutProdutos = new HashSet<LayoutProdutos>();
        }

        [Key]
        public int Id { get; set; }

        public int LayoutMercadosId { get; set; }

        [ForeignKey("LayoutMercadosId")]
        public virtual LayoutMercados LayoutMercados { get; set; }

        [Required]
        public string URL { get; set; }
        public Categoria Categoria { get; set; }
        public virtual ICollection<LayoutProdutos> LayoutProdutos { get; set; }
    }
}
