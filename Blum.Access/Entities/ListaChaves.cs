﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("LISTA_CHAVES")]
    public class ListaChaves : EntityBase
    {
        [Key]
        public int Id { get; set; }
        public int ListasId { get; set; }

        [StringLength(50), DisplayName("Chave Produto")]
        public string ChaveProduto { get; set; }

        [ForeignKey("ListasId")]
        public virtual Listas Listas { get; set; }
    }
}
