﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("CIDADES")]
    public class Cidades
    {
        [Key]
        public int Id { get; set; }
        public int EstadosId { get; set; }
        public int PaisesId { get; set; }

        [Required, StringLength(255), DisplayName("Nome")]
        public string Nome { get; set; }

        [Required, StringLength(2), DisplayName("Sigla")]
        public string Sigla { get; set; }

        [ForeignKey("EstadosId")]
        public virtual Estados Estados { get; set; }

        [ForeignKey("PaisesId")]
        public virtual Paises Paises { get; set; }
    }
}
