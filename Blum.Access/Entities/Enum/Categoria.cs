﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Blum.Access.Entities.Enum
{
    public enum Categoria
    {
        Nenhum = 0,
        [Description("Alimentos")]
        Alimentos,
        [Description("Bebidas")]
        Bebidas,
        [Description("Proteínas")]
        Proteinas,
        [Description("Bazar")]
        Bazar,
        [Description("Especiais")]
        Especiais,
        [Description("Frios")]
        Frios,
        [Description("Higiene")]
        Higiene,
        [Description("Hortifruti")]
        Hortifruti,
        [Description("Limpeza")]
        Limpeza,
        [Description("Pet")]
        Pet,
        [Description("Padaria")]
        Padaria
    }
}
