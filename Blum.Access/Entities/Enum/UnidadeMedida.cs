﻿using System.ComponentModel;

namespace Blum.Access.Entities.Enum
{
    public enum UnidadeMedida
    {
        Nenhum = 0,
        [Description("Kg")]
        Quilo,
        [Description("g")]
        Grama,
        [Description("L")]
        Litro,
        [Description("ml")]
        Mililitro,
        [Description("UN")]
        Unitario
    }
}
