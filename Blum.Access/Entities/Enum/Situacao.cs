﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Blum.Access.Entities.Enum
{
    public enum Situacao
    {
        [Description("Ativo")]
        Ativo = 0,
        [Description("Inativo")]
        Inativo,
        [Description("Indisponível")]
        Indisponivel
    }
}
