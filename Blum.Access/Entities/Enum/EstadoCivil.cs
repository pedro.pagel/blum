﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blum.Access.Entities.Enum
{
    public enum EstadoCivil
    {
        Nenhum = 0,
        [Display(Name = "Solteiro(a)")]
        Solteiro,
        [Display(Name = "Casado(a)")]
        Casado,
        [Display(Name = "Viúvo(a)")]
        Viuvo,
        [Display(Name = "Divorciado(a)")]
        Divorciado,
        [Display(Name = "Amasiado(a)")]
        Amasiado
    }
}
