﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Blum.Access.Entities.Enum
{
    public enum SimNao
    {
        [Description("Sim")]
        Sim = 0,
        [Description("Não")]
        Nao
    }
}
