﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blum.Access.Entities.Enum
{
    public enum Sexo
    {
        Nenhum = 0,
        [Display(Name = "Masculino")]
        Masculino,
        [Display(Name = "Feminino")]
        Feminino
    }
}
