﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("LISTA_PRODUTOS")]
    public class ListaProdutos : EntityBase
    {
        [Key]
        public int Id { get; set; }

        public int ProdutosId { get; set; }
        public int? ListaChaveId { get; set; }
        public int? ListasId { get; set; }

        [ForeignKey("ProdutosId")]
        public virtual Produtos Produtos { get; set; }

        [ForeignKey("ListaChaveId")]
        public virtual ListaChaves ListaChaves { get; set; }

        [ForeignKey("ListasId")]
        public virtual Listas Listas { get; set; }
    }
}
