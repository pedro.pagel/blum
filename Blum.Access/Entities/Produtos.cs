﻿using Blum.Access.Entities.Enum;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("PRODUTOS")]
    public class Produtos : EntityBase
    {
        public Produtos()
        {
            this.Situacao = Situacao.Ativo;
            this.UnidadeMedida = UnidadeMedida.Nenhum;
        }

        [Key]
        public int Id { get; set; }

        public int MercadosId { get; set; }
        public int SubCategoriaId { get; set; }
        public int? MarcasId { get; set; }

        [Required, StringLength(255), DisplayName("Nome")]
        public string Nome { get; set; }

        [Required, DisplayName("Categoria")]
        public Categoria Categoria { get; set; }

        [DisplayName("Unidade de Medida")]
        public UnidadeMedida UnidadeMedida { get; set; }

        [Required, DisplayName("Preço"), Column(TypeName = "money")]
        public decimal Preco { get; set; }

        [Required, DisplayName("Preço Original"), Column(TypeName = "money")]
        public decimal PrecoOriginal { get; set; }

        [Required, DisplayName("Preço Exclusivo"), Column(TypeName = "money")]
        public decimal PrecoExclusivo { get; set; }

        [DisplayName("Último Preço"), Column(TypeName = "money")]
        public decimal UltimoPreco { get; set; }

        [DisplayName("Último Preço Original"), Column(TypeName = "money")]
        public decimal UltimoPrecoOriginal { get; set; }

        [DisplayName("Último Preço Exclusivo"), Column(TypeName = "money")]
        public decimal UltimoPrecoExclusivo { get; set; }

        [ForeignKey("SubCategoriaId")]
        public virtual SubCategorias SubCategorias { get; set; }

        [ForeignKey("MercadosId")]
        public virtual Mercados Mercados { get; set; }

        [StringLength(255)]
        public string IdAnuncio { get; set; }

        public Situacao Situacao { get; set; }
        public DateTime DataInclusao { get; set; }
        public DateTime? DataAlteracao { get; set; }
        public byte Kit { get; set; }
        public long Popularidade { get; set; }
    }
}
