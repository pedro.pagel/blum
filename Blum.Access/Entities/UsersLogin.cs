﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blum.Access.Entities
{
    [Table("UsersLogin")]
    public class UsersLogin : EntityBase
    {
        [Key]
        public int Id { get; set; }

        [Required, StringLength(255), DisplayName("E-mail")]
        public string Email { get; set; }

        [Required, StringLength(8), DisplayName("Senha")]
        public string Senha { get; set; }
    }
}
