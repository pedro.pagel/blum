﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blum.Access.Entities
{
    [Table("LISTAS")]
    public class Listas : EntityBase
    {
        [Key]
        public int Id { get; set; }
        public int PessoasId { get; set; }

        public DateTime DataInclusao { get; set; }
        public DateTime? DataAlteracao { get; set; }

        [ForeignKey("PessoasId")]
        public virtual Pessoas Pessoas { get; set; }
    }
}
