﻿using Blum.Access;
using Blum.Business.Negocio.Leitura;
using System;

namespace Blum.Reader
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine("Inicio");
            Console.WriteLine(DateTime.Now);
            BlumAccess.Db = new BlumConnection();

            Leitura LeituraHtml = new Leitura();
            LeituraHtml.Processar();

            Console.WriteLine("FIM");
            Console.WriteLine(DateTime.Now);
        }
    }
}