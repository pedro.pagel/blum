﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Blum.Access;
using Blum.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Blum.Web.Controllers
{
    public class ConsultaController : Controller
    {
        // GET: Consulta
        public ActionResult Index()
        {
            return View();
        }

        // GET: Consulta/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public async Task<IActionResult> Produtos(ConsultaViewModel model)
        {
            if (model.Buscar != null)
            {
                using (var context = new BlumConnection())
                {
                    var produtos = await context.Produtos.Include("Mercados").Where(p => p.Nome.Contains(model.Buscar)).ToListAsync();

                    model.Produtos = produtos;
                }
            }

            return View(model);
        }
    }
}