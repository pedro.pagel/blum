﻿using Blum.Business.Services;
using Blum.Business.Services.Interfaces;
using Blum.Web.Controllers.Base;
using Blum.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Blum.Web.Controllers
{
    public class UsersLoginController : BaseController
    {
        private readonly IUsersLogin _usersLoginService;

        public UsersLoginController(IUsersLogin usersLoginService)
        {
            _usersLoginService = usersLoginService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Registrar()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Registrar(UsersLoginViewModel model)
        {
            UsersLogin usersLogin = new UsersLogin();
            usersLogin.Clone(model);

            //_usersLoginService.Gravar(usersLogin);
            this.Success("OK");
            return View();
        }
    }
}
