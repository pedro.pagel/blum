﻿using Blum.Access.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blum.Web.Models
{
    public class ConsultaViewModel : BaseViewModel
    {
        [Required, StringLength(255), DisplayName("Buscar")]
        public string Buscar { get; set; }

        public List<Produtos> Produtos { get; set; }
    }
}
