﻿using Blum.Access.Entities.Enum;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Blum.Web.Models
{
    public class CadastroViewModel : BaseViewModel
    {
        [Required, StringLength(255), DisplayName("Nome")]
        public string Nome { get; set; }

        [Required, StringLength(255), DisplayName("E-mail")]
        public string Email { get; set; }

        [Required, StringLength(15), DisplayName("CPF")]
        public string CPF { get; set; }

        [DisplayName("Sexo")]
        public Sexo Sexo { get; set; }

        [DisplayName("Estado Civil")]
        public EstadoCivil? EstadoCivil { get; set; }

        [Required, DisplayName("Data de Nascimento")]
        public DateTime DataNascimento { get; set; }
        public DateTime DataInclusao { get; set; }
    }
}
