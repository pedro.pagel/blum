﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Blum.Web.Models
{
    public class UsersLoginViewModel : BaseViewModel
    {
        [Required, StringLength(8), DisplayName("Senha")]
        public string Senha { get; set; }

        [Required, StringLength(8), DisplayName("Confirmar Senha")]
        public string ConfirmarSenha { get; set; }

        [Required, StringLength(255), DisplayName("E-mail")]
        public string Email { get; set; }

        [Required, StringLength(255), DisplayName("Confirmar E-mail")]
        public string ConfirmarEmail { get; set; }
    }
}
