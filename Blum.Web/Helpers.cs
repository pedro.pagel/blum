﻿using Blum.Web.Models;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blum.Web
{
    public static class Base
    {
        public static BaseViewModel Model = null;
        public static bool Linha = false;
    }

    public enum InputSize
    {
        None = 0,
        Small = 2,
        MidSize = 6,
        Large = 12
    }

    public enum ButtonType
    {
        Default = 0,
        Primary,
        Secondary,
        Info,
        Success,
        Danger,
        Warning
    }

    public enum EditRequired
    {
        Default = 0,
        Yes,
        No
    }

    class HtmlHelperAtributes
    {
        public string Id { get; set; }
        public string MaxLength { get; set; }
        public string DisplayName { get; set; }
        public string Type { get; set; }
        public bool Required { get; set; }
    }

    public static class Helpers
    {
        public static IHtmlContent CreateModel(this IHtmlHelper helper, BaseViewModel model)
        {
            Base.Model = model;

            return new HtmlString(string.Empty);
        }

        public static IHtmlContent BotaoSalvar(this IHtmlHelper helper, string display)
        {
            return new HtmlString(string.Format(
                "<div class=\"col-lg-12 text-center\">" +
                        "<button class=\"btn btn-success mt-4 mb-4\" type=\"submit\">{0}</button>" +
                "</div>", display));
        }

        public static IHtmlContent Botao(this IHtmlHelper helper, string display, string buttonAction, ButtonType buttonType = ButtonType.Default)
        {
            string type = "btn btn-default";

            switch (buttonType)
            {
                case ButtonType.Danger:
                    {
                        type = "btn btn-danger";
                        break;
                    }

                case ButtonType.Default:
                    {
                        type = "btn btn-default";
                        break;
                    }

                case ButtonType.Info:
                    {
                        type = "btn btn-info";
                        break;
                    }

                case ButtonType.Primary:
                    {
                        type = "btn btn-primary";
                        break;
                    }

                case ButtonType.Secondary:
                    {
                        type = "btn btn-secondary";
                        break;
                    }

                case ButtonType.Success:
                    {
                        type = "btn btn-success";
                        break;
                    }

                case ButtonType.Warning:
                    {
                        type = "btn btn-warning";
                        break;
                    }
            }
            
            return new HtmlString(string.Format(
                "<div class=\"col-lg-12 text-center\">" +
                        "<button asp-action=\"{0}\" class=\"{1} mt-4 mb-4\" data-toggle=\"sweet-alert\" data-sweet-alert=\"success\" type=\"submit\">{2}</button>" +
                "</div>", buttonAction, type, display));
        }

        public static IHtmlContent Linha(this IHtmlHelper helper)
        {
            Base.Linha = true;
            return new HtmlString("<div class=\"row\"> ");
        }

        public static IHtmlContent FimLinha(this IHtmlHelper helper)
        {
            Base.Linha = false;
            return new HtmlString(" </div>");
        }

        private static HtmlHelperAtributes GetHelper(string field)
        {
            Type type = Base.Model.GetType();

            var atributes = type.GetProperties().Where(p => p.Name == field).SingleOrDefault().CustomAttributes.ToList();
            HtmlHelperAtributes htmlHelper = new HtmlHelperAtributes()
            {
                Id = "id=\"input={1}\""
            };

            foreach (var item in atributes)
            {
                if (item.AttributeType.Name == "DisplayNameAttribute")
                {
                    htmlHelper.DisplayName = item.ConstructorArguments.SingleOrDefault().Value.ToString();
                }
                else if (item.AttributeType.Name == "StringLengthAttribute")
                {
                    htmlHelper.MaxLength = item.ConstructorArguments.SingleOrDefault().Value.ToString();
                }
                else if (item.AttributeType.Name == "RequiredAttribute")
                {
                    htmlHelper.Required = true;
                }
            }

            if (type.GetProperties().Where(p => p.Name == field).SingleOrDefault().PropertyType.Name.ToLower() == "string")
            {
                htmlHelper.Type = "text";
            }
            else if (type.GetProperties().Where(p => p.Name == field).SingleOrDefault().PropertyType.Name.ToLower() == "decimal")
            {
                htmlHelper.Type = "number";
            }
            else if (type.GetProperties().Where(p => p.Name == field).SingleOrDefault().PropertyType.Name.ToLower() == "int")
            {
                htmlHelper.Type = "number";
            }
            else if ((type.GetProperties().Where(p => p.Name == field).SingleOrDefault().PropertyType.Name.ToLower() == "date") || (type.GetProperties().Where(p => p.Name == field).SingleOrDefault().PropertyType.Name.ToLower() == "datetime"))
            {
                htmlHelper.Type = "text";
                htmlHelper.Id = "id=\"datepicker\"";
            }

            return htmlHelper;
        }

        public static IHtmlContent Date(this IHtmlHelper helper, string field)
        {
            HtmlHelperAtributes htmlHelper = GetHelper(field);
            StringBuilder stringBuilder = new StringBuilder();

            if (!Base.Linha)
            {
                stringBuilder.Append("<div class=\"row\">");
            }

            stringBuilder.Append("<div class=\"col-lg-2\">");
            stringBuilder.Append("<div class=\"form-group\">");
            stringBuilder.Append($"<label class=\"form-control-label\" for=\"input-{field}\">{htmlHelper.DisplayName}</label>");
            stringBuilder.Append("<div class=\"input-group input-group-alternative\">");
            stringBuilder.Append("<div class=\"input-group-prepend\">");
            stringBuilder.Append("<span class=\"input-group-text\"><i class=\"ni ni-calendar-grid-58\"></i></span>");
            stringBuilder.Append("</div>");
            stringBuilder.Append($"<input id=\"datepicker\" name=\"{field}\" type=\"text\" class=\"form-control format=\"dd/MM/yyyy\" form-control-alternative\"/>");
            stringBuilder.Append("</div>");
            stringBuilder.Append("</div>");
            stringBuilder.Append("</div>");

            if (!Base.Linha)
            {
                stringBuilder.Append("</div>");
            }

            return new HtmlString(stringBuilder.ToString());
        }

        public static IHtmlContent EditCPF(this IHtmlHelper helper, string field)
        {
            HtmlHelperAtributes htmlHelper = GetHelper(field);

            StringBuilder stringBuilder = new StringBuilder();

            if (!Base.Linha)
            {
                stringBuilder.Append("<div class=\"row\">");
            }

            stringBuilder.Append($"<div class=\"col-lg-2\">");
            stringBuilder.Append("<div class=\"form-group\">");
            stringBuilder.Append($"<label class=\"form-control-label\" for=\"input-{field}\">{htmlHelper.DisplayName}</label>");
            stringBuilder.Append($"<input id=\"cpf\" name=\"{field}\" type=\"{htmlHelper.Type}\" placeholder=\"{htmlHelper.DisplayName}\" class=\"form-control form-control-alternative\" onkeypress=\"$(this).mask('000.000.000-00');\"");

            if (htmlHelper.Required)
            {
                stringBuilder.Append(" required");
            }

            if (!string.IsNullOrWhiteSpace(htmlHelper.MaxLength))
            {
                stringBuilder.Append($" maxLength=\"{htmlHelper.MaxLength}\"");
            }

            stringBuilder.Append("/>");
            stringBuilder.Append("</div>");
            stringBuilder.Append("</div>");

            if (!Base.Linha)
            {
                stringBuilder.Append("</div>");
            }

            return new HtmlString(stringBuilder.ToString());
        }

        public static IHtmlContent EditPassword(this IHtmlHelper helper, string field, EditRequired required = EditRequired.Default)
        {
            HtmlHelperAtributes htmlHelper = GetHelper(field);

            StringBuilder stringBuilder = new StringBuilder();

            if (!Base.Linha)
            {
                stringBuilder.Append("<div class=\"row\">");
            }

            stringBuilder.Append($"<div class=\"col-lg-{(int)2}\">");
            stringBuilder.Append("<div class=\"form-group\">");
            stringBuilder.Append($"<label class=\"form-control-label\" for=\"input-{field}\">{htmlHelper.DisplayName}</label>");
            stringBuilder.Append($"<input name=\"{field}\" type=\"password\" placeholder=\"{htmlHelper.DisplayName}\" class=\"form-control form-control-alternative\"");

            if (((required == EditRequired.Default) && (htmlHelper.Required)) || (required == EditRequired.Yes))
            {
                stringBuilder.Append(" required");
            }

            if (!string.IsNullOrWhiteSpace(htmlHelper.MaxLength))
            {
                stringBuilder.Append($" maxLength=\"{htmlHelper.MaxLength}\"");
            }

            stringBuilder.Append($" onpaste=\"return false\" ondrop=\"return false\"/>");
            stringBuilder.Append("</div>");
            stringBuilder.Append("</div>");

            if (!Base.Linha)
            {
                stringBuilder.Append("</div>");
            }

            return new HtmlString(stringBuilder.ToString());
        }

        public static IHtmlContent Edit(this IHtmlHelper helper, string field, InputSize size = InputSize.MidSize, bool canPaste = true, EditRequired required = EditRequired.Default)
        {
            HtmlHelperAtributes htmlHelper = GetHelper(field);
            string blockPaste = canPaste ? string.Empty : "onpaste=\"return false\" ondrop=\"return false\" autocomplete=\"rutjfkde\"";

            StringBuilder stringBuilder = new StringBuilder();

            if (!Base.Linha)
            {
                stringBuilder.Append("<div class=\"row\">");
            }

            stringBuilder.Append($"<div class=\"col-lg-{(int)size}\">");
            stringBuilder.Append("<div class=\"form-group\">");
            stringBuilder.Append($"<label class=\"form-control-label\" for=\"input-{field}\">{htmlHelper.DisplayName}</label>");
            stringBuilder.Append($"<input name=\"{field}\" type=\"{htmlHelper.Type}\" placeholder=\"{htmlHelper.DisplayName}\" class=\"form-control form-control-alternative\"");
 
            if (((required == EditRequired.Default) && (htmlHelper.Required)) || (required == EditRequired.Yes))
            {
                stringBuilder.Append(" required");
            }

            if (!string.IsNullOrWhiteSpace(htmlHelper.MaxLength))
            {
                stringBuilder.Append($" maxLength=\"{htmlHelper.MaxLength}\"");
            }

            stringBuilder.Append($" {blockPaste}/>");
            stringBuilder.Append("</div>");
            stringBuilder.Append("</div>");

            if (!Base.Linha)
            {
                stringBuilder.Append("</div>");
            }

            return new HtmlString(stringBuilder.ToString());
        }

        public static IHtmlContent EnumEdit(this IHtmlHelper helper, string field, IEnumerable<SelectListItem> enumList, InputSize size = InputSize.Small)
        {
            StringBuilder stringBuilder = new StringBuilder();
            HtmlHelperAtributes htmlHelper = GetHelper(field);

            if (!Base.Linha)
            {
                stringBuilder.Append("<div class=\"row\">");
            }

            stringBuilder.Append("<div class=\"col-lg-{0}\">" +
                    "<div class=\"form-group\">" +
                        "<label class=\"form-control-label\" for=\"input-{1}\">{2}</label>");

            stringBuilder.Append($"<select name=\"{field}\" class=\"form-control form-control-alternative\">");

            foreach (var item in enumList)
            {
                stringBuilder.Append($"<option value=\"{item.Value}\">{item.Text}</option>");
            }

            stringBuilder.Append("</select>");
            stringBuilder.Append("<span class=\"invalid-feedback field-validation-valid\" style=\"display: block\" data-valmsg-for=\"Input.{1}\" data-valmsg-replace=\"true\"></span>" +
                "</div> " +
            "</div>");

            if (!Base.Linha)
            {
                stringBuilder.Append("</div>");
            }

            return new HtmlString(string.Format(stringBuilder.ToString(), (int)size, field, htmlHelper.DisplayName));
        }
    }
}
