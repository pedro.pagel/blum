﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Blum.Business.Negocio.Funcoes
{
    public class Funcoes
    {
        public static List<string> GetEnumDescriptions(Type enumType)
        {
            FieldInfo field;
            DescriptionAttribute[] attributes;
            List<string> listaCampos = new List<string>();

            foreach (var item in System.Enum.GetValues(enumType))
            {
                field = item.GetType().GetField(item.ToString());
                attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                {
                    listaCampos.Add(attributes[0].Description);
                }
            }

            return listaCampos;
        }

        public static DateTime GetSqlDateTimeNow()
        {
            return DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
        }

        public static DateTime GetSqlDateTime(string data)
        {
            return DateTime.ParseExact(data, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
        }

        public static Exception BusinessException(string value)
        {
            throw new Exception(value);
        }
    }
}
