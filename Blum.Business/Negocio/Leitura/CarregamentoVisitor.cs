﻿using Blum.Business.Negocio.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace Blum.Business.Negocio.Leitura
{
    public class CarregamentoVisitor : IVisitor
    {
        private HttpWebRequest MontarRequest(string url)
        {
            var cookieContainer = new CookieContainer();

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);

            request.CookieContainer = cookieContainer;
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

            return request;
        }


        public void Visit(LeitorPorRequisicao leitorPorRequisicao)
        {
            Leitor leitor = leitorPorRequisicao as Leitor;

            HttpWebRequest requestTentativa = this.MontarRequest(leitor.URL);
            using (WebResponse response = requestTentativa.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    leitor.HTML = sr.ReadToEnd();
                }
            }

            HttpWebRequest request = this.MontarRequest(leitor.URL);

            //Somente para um atualmente, tem que ver como funciona o ContentLength com mais itens
            //string data = $"{leitorPorRequisicao.Chave} = {leitorPorRequisicao.Valor}";
            string data = $"zipCode=89035-450";
            byte[] bytes = Encoding.UTF8.GetBytes(data);

            request.ContentLength = bytes.Length;

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(bytes, 0, bytes.Length);
                dataStream.Close();
            }

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    leitor.HTML = sr.ReadToEnd();
                }
            }
        }

        public void Visit(LeitorPadrao leitorPadrao)
        {
            Leitor leitor = leitorPadrao as Leitor;
            HttpWebRequest request = this.MontarRequest(leitor.URL);

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    leitor.HTML = sr.ReadToEnd();
                }
            }
        }

        public void Visit(LeitorBrowser leitorBrowser)
        {
            Leitor leitor = leitorBrowser as Leitor;

            var chromeDriver = leitorBrowser.Driver;
            chromeDriver.Navigate().GoToUrl(leitor.URL);

            if (!leitorBrowser.LoadedContent)
            {
                IJavaScriptExecutor executor = (IJavaScriptExecutor)chromeDriver;
                executor.ExecuteScript("document.getElementById('txtStoreId').setAttribute('value', '89035-450');");
                executor.ExecuteScript("jQuery('#btnFancybox').click();");

                this.Wait(2000);
                executor.ExecuteScript("jQuery('#btAccessStore.btn.send.ir').click();");

                this.Wait(2000);
                leitorBrowser.LoadedContent = true;
            }

            leitor.HTML = chromeDriver.PageSource;
        }

        public void Wait(int time)
        {
            Thread.Sleep(time);
        }
    }

    public class LeitorPorRequisicao : Leitor
    {
        public string Chave { get; set; }
        public string Valor { get; set; }

        public override void Accept(IVisitor visitor)
        {
            IVisitor concreteVisitor = visitor as IVisitor;
            concreteVisitor.Visit(this);
        }
    }

    public class LeitorPadrao : Leitor
    {
        public override void Accept(IVisitor visitor)
        {
            IVisitor concreteVisitor = visitor as IVisitor;
            concreteVisitor.Visit(this);
        }
    }

    public class LeitorBrowser : Leitor
    {
        private ChromeDriver driver;
        private int ContadorDriver;

        public ChromeDriver Driver
        {
            get
            {
                ContadorDriver--;

                if (ContadorDriver == 0)
                {
                    this.LiberarDriver();
                }

                return driver;
            }
            set { driver = value; }
        }
        public bool LoadedContent { get; set; }

        public LeitorBrowser()
        {
            this.CriarDriver();
        }

        private void CriarDriver()
        {
            this.ContadorDriver = 5;

            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;

            ChromeOptions options = new ChromeOptions();
            options.AddArgument("headless");

            this.driver = new ChromeDriver(service, options);
            this.LoadedContent = false;
        }

        public override void Accept(IVisitor visitor)
        {
            IVisitor concreteVisitor = visitor as IVisitor;
            concreteVisitor.Visit(this);
        }

        public void LiberarDriver()
        {
            this.driver.Quit();
            this.CriarDriver();
        }
    }
}
