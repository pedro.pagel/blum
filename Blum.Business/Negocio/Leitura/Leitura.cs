﻿using Blum.Access;
using Blum.Access.Entities;
using Blum.Access.Entities.Enum;
using Blum.Business.Negocio.Interfaces;
using Blum.Business.Negocio.Produto;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Blum.Business.Negocio.Leitura
{
    public class Leitura
    {
        private enum Precos
        {
            Final = 0,
            Original,
            Exclusivo
        }

        private ProdutoBusiness ProdutoBusiness { get; set; }
        private List<Leitor> ListLeitor = new List<Leitor>();
        private List<SubCategorias> SubCategorias { get; set; }
        private Leitor VisitanteCarregamento { get; set; }
        private Produtos Produtos { get; set; }
        private int Pagina { get; set; }

        public void Attach(Leitor leitor)
        {
            ListLeitor.Add(leitor);
        }

        public void Detach(Leitor leitor)
        {
            ListLeitor.Remove(leitor);
        }

        public void Accept(IVisitor visitor)
        {
            foreach (Leitor leitor in ListLeitor)
            {
                leitor.Accept(visitor);
            }
        }

        public Leitura()
        {
            this.SubCategorias = BlumAccess.Db.SubCategorias.Where(s => s.Id > 0).ToList();
            this.ProdutoBusiness = new ProdutoBusiness();
        }

        private void CarregarVisitanteCarregamento(int idLayout)
        {
            RequisicaoValores requisicaoValores = BlumAccess.Db.RequisicaoValores.Where(r => r.LayoutMercadosId == idLayout).FirstOrDefault();

            if ((requisicaoValores != null) && (requisicaoValores.Id > 0))
            {
                this.VisitanteCarregamento = new LeitorBrowser();
            }
            else
            {
                this.VisitanteCarregamento = new LeitorPadrao();
            }

            this.Attach(this.VisitanteCarregamento);
        }

        public void Processar()
        {
            var layoutList = (from mercado in BlumAccess.Db.Mercados
                                               join layoutMercados in BlumAccess.Db.LayoutMercados on mercado.Id equals layoutMercados.MercadosId
                                               join layoutCategorias in BlumAccess.Db.LayoutCategorias on layoutMercados.Id equals layoutCategorias.LayoutMercadosId
                                               join layoutProdutos in BlumAccess.Db.LayoutProdutos on layoutMercados.Id equals layoutProdutos.LayoutMercadosId
                                               where
                                                   mercado.Id == 2 
                                               select new { layoutProdutos.Id, layoutProdutos, layoutCategorias, layoutMercados }).OrderByDescending(m => m.Id).ToList();
            
            foreach (var item in layoutList)
            {
                this.Pagina = 0;
                this.CarregarVisitanteCarregamento(item.layoutMercados.Id);
                this.ProdutoBusiness.Mercado = item.layoutMercados.Id;

                while (this.Pagina != -1)
                {
                    this.LerPaginasHtml(item.layoutProdutos, item.layoutCategorias, this.Pagina);
                }

                this.Detach(this.VisitanteCarregamento);
            }
        }

        private string CarregarHtml(LayoutProdutos layoutProdutos, LayoutCategorias layoutCategorias, int pagina)
        {
            CarregamentoVisitor carregamentoHtmlVisitor = new CarregamentoVisitor();
            this.VisitanteCarregamento.URL = string.Format(layoutCategorias.URL, pagina);

            this.Accept(carregamentoHtmlVisitor);

            return this.VisitanteCarregamento.HTML;
        }

        //Cases: 
        //Cooper precisa so da chave
        //Bistek precisa de uma chave e do nome node 
        //Angeloni precisa da chave e do conteudo do node
        //Essas situações devem cobrir boa parte das necessidades futuras
        private List<HtmlNode> CarregarNodes(HtmlDocument htmlDoc, LayoutProdutos layoutProdutos)
        {
            if (!string.IsNullOrEmpty(layoutProdutos.Chave) && string.IsNullOrEmpty(layoutProdutos.TagChave) && string.IsNullOrEmpty(layoutProdutos.ConteudoChave))
            {
                return htmlDoc.DocumentNode.Descendants(layoutProdutos.Chave).ToList();
            }
            else if (!string.IsNullOrEmpty(layoutProdutos.TagChave) && !string.IsNullOrEmpty(layoutProdutos.Chave))
            {
                return htmlDoc.DocumentNode.Descendants(layoutProdutos.Chave).Where(n => n.ChildNodes.Where(a => a.Name.Equals(layoutProdutos.TagChave)).Count() > 0).ToList();
            }
            else
            {
                return htmlDoc.DocumentNode.SelectSingleNode(layoutProdutos.NodeUnico).Descendants(layoutProdutos.Chave).Where(n => n.OuterHtml.Contains(layoutProdutos.ConteudoChave)).ToList();
            }
        }

        private void LerPaginasHtml(LayoutProdutos layoutProdutos, LayoutCategorias layoutCategorias, int paginaLimite)
        {
            HtmlDocument htmlDoc = new HtmlDocument();
            try
            {
                while (this.Pagina < (paginaLimite + 25))
                {
                    this.Pagina++;
                    string html = this.CarregarHtml(layoutProdutos, layoutCategorias, this.RetornarNumeroPagina(layoutCategorias));

                    htmlDoc.LoadHtml(html);

                    List<HtmlNode> htmlNodes = CarregarNodes(htmlDoc, layoutProdutos);

                    if (htmlNodes.Count == 0)
                    {
                        Pagina = -1;
                        break;
                    }

                    this.AdicionarProdutos(htmlNodes, layoutProdutos, layoutCategorias);
                }

                BlumAccess.Db.SaveChanges();
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        //Determina o numero da pagina, se tiver a flag, entao multiplica pela quantidade
        private int RetornarNumeroPagina(LayoutCategorias layoutCategorias)
        {
            if (layoutCategorias.LayoutMercados.PossuiNumeroPagina)
            {
                if (this.Pagina == 1)
                {
                    return 0;
                }
                else
                {
                    return this.Pagina * layoutCategorias.LayoutMercados.QuantidadePorPagina;
                }
            }

            return this.Pagina;
        }

        private void AdicionarProdutos(List<HtmlNode> htmlNodes, LayoutProdutos layoutProdutos, LayoutCategorias layoutCategorias)
        {
            foreach (HtmlNode htmlNode in htmlNodes)
            {
                this.Produtos = new Produtos();

                HtmlNode node = this.VerificarNode(htmlNode.Descendants(layoutProdutos.TagNome).ToList(), layoutProdutos.Nome);

                this.Produtos.Nome = HttpUtility.HtmlDecode(node.InnerText.Trim());
                this.Produtos.DataInclusao = DateTime.Now;
                this.Produtos.MercadosId = layoutCategorias.LayoutMercados.MercadosId;
                this.Produtos.Categoria = layoutCategorias.Categoria;
                this.Produtos.SubCategoriaId = ConsistirSubCategoria(node);
                this.Produtos.IdAnuncio = this.ConsistirId(htmlNode, node, layoutProdutos.TagAnuncio, layoutProdutos.IdAnuncio);
                this.Produtos.Preco = this.ConsistirPreco(htmlNode, Precos.Final, layoutProdutos);
                this.Produtos.PrecoOriginal = this.ConsistirPreco(htmlNode, Precos.Original, layoutProdutos);
                this.Produtos.PrecoExclusivo = this.ConsistirPreco(htmlNode, Precos.Exclusivo, layoutProdutos);

                if (this.ConsistirDisponibilidade(this.Produtos, htmlNode, layoutProdutos.TagIndisponivel, layoutProdutos.IdIndisponivel))
                {
                    this.Produtos.Situacao = Situacao.Indisponivel;
                }

                this.ProdutoBusiness.Processar(this.Produtos);
            }
        }

        private HtmlNode VerificarNode(List<HtmlNode> nodeList, string chave, string badOuterHtml = null)
        {
            if (string.IsNullOrEmpty(badOuterHtml))
            {
                return nodeList.Find(x => x.OuterHtml.Contains(chave));
            }
            else
            {
                return nodeList.Find(x => x.OuterHtml.Contains(chave) && !(x.OuterHtml.Contains(badOuterHtml)));
            }
        }

        private int ConsistirSubCategoria(HtmlNode node)
        {
            SubCategorias subCategorias = SubCategorias.Find(s => node.InnerText.Trim().ToLower().StartsWith(s.Nome));

            //if (subCategorias == null)
            //{
            //    subCategorias = SubCategorias.Find(s => node.InnerText.Trim().ToLower().Contains(s.Nome));
            //}

            if (subCategorias != null)
            {
                return subCategorias.Id;
            }

            //Sempre vai retornar outros quando nao achar
            return this.SubCategorias.Find(s => s.Nome == "outros").Id;
        }

        private bool ConsistirDisponibilidade(Produtos produtos, HtmlNode htmlNodes, string tag, string chave)
        {
            string retorno = string.Empty;

            if ((produtos.Preco <= 0) && (produtos.PrecoExclusivo <= 0) && (produtos.PrecoOriginal <= 0))
            {
                HtmlNode node = this.VerificarNode(htmlNodes.Descendants(tag).ToList(), chave);
                retorno = node.InnerText.Trim();
            }

            return !string.IsNullOrEmpty(retorno);
        }

        private string ConsistirId(HtmlNode htmlNode, HtmlNode childNode, string tag, string chave)
        {
            HtmlNode node = this.VerificarNode(childNode.Descendants(tag).ToList(), chave);

            if (node == null)
            {
                node = this.VerificarNode(htmlNode.Descendants(tag).ToList(), chave);
            }

            return (node == null ? string.Empty : node.Id);
        }

        private decimal ConsistirPreco(HtmlNode htmlNodes, Precos precos, LayoutProdutos layoutProdutos)
        {
            string tag = string.Empty;
            string chave = string.Empty;
            string badOuterHtml = string.Empty;

            switch (precos)
            {
                case Precos.Exclusivo:
                    tag = layoutProdutos.TagPrecoExclusivo;
                    chave = layoutProdutos.PrecoExclusivo;
                    break;

                case Precos.Original:
                    tag = layoutProdutos.TagPrecoOriginal;
                    chave = layoutProdutos.PrecoOriginal;

                    break;

                case Precos.Final:
                    tag = layoutProdutos.TagPrecoFinal;
                    chave = layoutProdutos.PrecoFinal;
                    badOuterHtml = layoutProdutos.BadOuterHtmlPreco;
                    break;
            }


            if (string.IsNullOrEmpty(tag) && string.IsNullOrEmpty(chave))
            {
                return 0;
            }

            HtmlNode node = null;

            if ((precos == Precos.Original) && (!string.IsNullOrEmpty(layoutProdutos.PrecoOriginalXPath)))
            {
                if (htmlNodes.OuterHtml.Contains(layoutProdutos.PrecoOriginal))
                {
                    node = htmlNodes.SelectSingleNode(layoutProdutos.PrecoOriginalXPath);
                }
            }
            else
            {
                node = this.VerificarNode(htmlNodes.Descendants(tag).ToList(), chave, badOuterHtml);
            }

            return (node == null || string.IsNullOrWhiteSpace(node.InnerText.Trim()) ? 0 : decimal.Parse(Regex.Replace(node.InnerText.Trim(), @"[^0-9:,]+", "")));
        }
    }
}
