﻿using Blum.Business.Negocio.Interfaces;

namespace Blum.Business.Negocio.Leitura
{
    public class Leitor : Element
    {
        public string HTML { get; set; }
        public string URL { get; set; }
        public long Layout { get; set; }

        public override void Accept(IVisitor visitor)
        {

        }
    }
}
