﻿using Blum.Access;
using Blum.Access.Entities;
using Blum.Access.Entities.Enum;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Blum.Business.Negocio.Lista
{
    class RegexFiltros : Regex
    {
        public int QuantidadeFiltros { get; set; }

        public RegexFiltros(string pattern, int quantidade) : base(pattern)
        {
            this.QuantidadeFiltros = quantidade;
        }

    }

    public class ListaBusiness
    {
        private List<Produtos> ListaProdutos { get; set; }

        public ListaBusiness()
        {
            ListaProdutos = BlumAccess.Db.Produtos.Where(p => p.Situacao == Situacao.Ativo).ToList();
        }

        private RegexFiltros MontarFiltroRegex(string busca)
        {
            string[] dados = busca.Replace(" ", ";").Split(";").Where(d => !string.IsNullOrEmpty(d)).ToArray();
            return new RegexFiltros(string.Join("|", dados).ToLower(), dados.Length);
        }

        public List<Produtos> BuscarProduto(string chaves, string marcas, string filtroEspecifico)
        {
            RegexFiltros regexChaves = MontarFiltroRegex(chaves);

            var retornoProdutos = ListaProdutos.Where(p => regexChaves.Matches(p.Nome.ToLower()).Count == regexChaves.QuantidadeFiltros).ToList();

            if (!string.IsNullOrEmpty(marcas))
            {
                RegexFiltros regexMarcas = MontarFiltroRegex(marcas);
                retornoProdutos = retornoProdutos.Where(p => regexMarcas.IsMatch(p.Nome.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(filtroEspecifico))
            {
                RegexFiltros regexEspecifico = MontarFiltroRegex(filtroEspecifico);
                retornoProdutos = retornoProdutos.Where(p => regexEspecifico.Matches(p.Nome.ToLower()).Count == regexEspecifico.QuantidadeFiltros).ToList();
            }

            return retornoProdutos.GroupBy(g => g.MercadosId).SelectMany(m => m.Take(3)).OrderBy(p => p.Preco).ToList();
        }

        public int SalvarBusca(string busca, int lista)
        {
            ListaChaves listaChaves = new ListaChaves()
            {
                ListasId = lista,
                ChaveProduto = busca
            };

            listaChaves.Insert();

            return listaChaves.Id;
        }

        public void SalvarProdutos(List<Produtos> produtos, int lista, int chave)
        {
            foreach (Produtos produto in produtos)
            {
                ListaProdutos listaProdutos = new ListaProdutos()
                {
                    ListasId = lista,
                    ProdutosId = produto.Id,
                    ListaChaveId = chave
                };

                listaProdutos.Insert();
            }
        }
    }
}
