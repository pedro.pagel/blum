﻿namespace Blum.Business.Negocio.Enum
{
    public enum Operador
    {
        Nenhum = 0,
        And,
        Or,
        Like
    }
}
