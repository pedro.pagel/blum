﻿namespace Blum.Business.Negocio.Interfaces
{
    public interface IElement
    {
        public void Accept(IVisitor visitor);
    }

    public abstract class Element : IElement
    {
        public abstract void Accept(IVisitor visitor);
    }
}
