﻿using Blum.Business.Negocio.Leitura;

namespace Blum.Business.Negocio.Interfaces
{
    public interface IVisitor
    {
        public void Visit(LeitorPadrao leitorPadrao);
        public void Visit(LeitorBrowser leitorBrowser);
        public void Visit(LeitorPorRequisicao leitorPorRequisicao);
    }
}
