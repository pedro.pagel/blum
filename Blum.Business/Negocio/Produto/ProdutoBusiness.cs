﻿using Blum.Access.Entities;
using Blum.Access;
using System.Collections.Generic;
using System.Linq;

namespace Blum.Business.Negocio.Produto
{
    public class ProdutoBusiness
    {
        private List<Produtos> ListaProdutos { get; set; }

        private int mercado = 0;
        public int Mercado
        {
            get
            {
                return mercado;
            }

            set
            {
                if (this.mercado != value)
                {
                    this.mercado = value;
                    ListaProdutos = BlumAccess.Db.Produtos.Where(p => p.Mercados.Id == value).ToList();
                }
            }
        }

        public void Processar(Produtos registro)
        {
            if (this.PrimeiraCargaMercado(registro))
            {
                registro.Insert();
            }
            else
            {
                Produtos produto = ListaProdutos.Find(p => p.IdAnuncio == registro.IdAnuncio && p.MercadosId == registro.MercadosId);

                //Se nao achou, tenta pelo nome 
                if (produto == null)
                {
                    produto = ListaProdutos.Find(p => p.Nome == registro.Nome && p.MercadosId == registro.MercadosId);
                }

                //se nao achou, mas achou com id diferente, provavelmente é o mesmo produto mas com novo modelo no mercado
                if ((produto == null) || ((produto != null) && (!produto.IdAnuncio.Equals(registro.IdAnuncio))))
                {
                    registro.Insert();
                }
                else
                {
                    if (produto.PrecoOriginal != registro.PrecoOriginal)
                    {
                        registro.UltimoPrecoOriginal = produto.PrecoOriginal;
                    }

                    if (produto.PrecoExclusivo != registro.PrecoExclusivo)
                    {
                        registro.UltimoPrecoExclusivo = produto.PrecoExclusivo;
                    }

                    if (produto.Preco != registro.Preco)
                    {
                        registro.UltimoPreco = produto.Preco;
                    }

                    produto.Update(registro);
                }

            }
        }

        private bool PrimeiraCargaMercado(Produtos produto)
        {
            return ((ListaProdutos == null) || (ListaProdutos.Count == 0));
        }
    }
}
