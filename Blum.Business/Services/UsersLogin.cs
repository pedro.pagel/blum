﻿using Blum.Access;
using Blum.Business.Negocio.Funcoes;
using Blum.Business.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blum.Business.Services
{
    public class UsersLogin : Access.Entities.UsersLogin, IUsersLogin
    {
        public string ConfirmarSenha { get; set; }
        public string ConfirmarEmail { get; set; }

        public void Gravar(UsersLogin usersLogin)
        {
            if (usersLogin.Email != usersLogin.ConfirmarEmail)
            {
                Funcoes.BusinessException("E-mails não conferem!");
            }

            if (usersLogin.Senha != usersLogin.ConfirmarSenha)
            {
                Funcoes.BusinessException("Senhas não conferem!");
            }

            usersLogin.Insert();
        }

        public Access.Entities.UsersLogin Logar()
        {
            return BlumAccess.Db.UsersLogin.Where(u => u.Email.Equals(this.Email)).FirstOrDefault();
        }

        public bool VerificarLogin()
        {
            var usersLogin = this.Logar();
            return (usersLogin != null) && usersLogin.Id > 0;
        }
    }
}
