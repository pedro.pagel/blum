﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blum.Business.Services.Interfaces
{
    public interface IUsersLogin
    {
        public void Gravar(UsersLogin usersLogin);
        public Access.Entities.UsersLogin Logar();
        public bool VerificarLogin();
    }
}
